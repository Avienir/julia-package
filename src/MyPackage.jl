module MyPackage

function golden_section_search(a,b,epsilon,f)
    k = 1
    p = 0.382
    while abs(a-b) > epsilon
        w1 = a+(1-p)*(b-a)
        w2 = a+p* (b-a)
        fw1 = f(w1)
        fw2 = f(w2)
        if f(w1) > f(w2)
            b = w1
            end
        if f(w2) > f(w1)
            a = w2
            end
        end
    return (a+b)/2

    end
export golden_section_search;

end # module
