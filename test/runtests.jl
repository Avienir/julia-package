using MyPackage, Test

@testset "golden_ratio" begin
    @test golden_section_search(0,1,0.001,x->x^2) == 0.00036626644287217054
    @test golden_section_search(0,1,0.0001,x->-x^2 +1)== 0.9999669828558284

end